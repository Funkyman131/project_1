package Hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class TableMain {
    public static void main(String[] args) {


        NewTable newTable = new NewTable();
        newTable.setId(1);
        newTable.setName("aaa");
        newTable.setSecondName("bbb");
        newTable.setNumber(4568);


        final SessionFactory sessionFactory = HibarnateUtils.getSessionFactory();
        final Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(newTable);
        session.getTransaction().commit();

        session.close();
        HibarnateUtils.shutDownSessionFactory();
    }
}
