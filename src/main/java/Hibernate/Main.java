package Hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class Main {
    public static void main(String[] args) {

        Person person = new Person();
        person.setId(1);
        person.setFirstName("Janusz");
        person.setLastName("Nowak");
        person.setPesel(98524547635L);
        person.setAge(23);
       /* person.setaBoolean(true);
        person.setBigDecimal(BigDecimal.valueOf(123456789));
        person.setDate(new Date(System.currentTimeMillis()));
        person.setTime(new Time(System.nanoTime()));
        person.setStreet("Mickiewicza");
        person.setHouseNumber(33);
        person.setFlatNumber(16);*/




        final SessionFactory sessionFactory = HibarnateUtils.getSessionFactory();
        final Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(person);
        session.getTransaction().commit();

        session.close();
        HibarnateUtils.shutDownSessionFactory();

    }
}
