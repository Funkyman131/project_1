package Hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


import java.util.ArrayList;
import java.util.List;

public class MainEncje {
    public static void main(String[] args) {

        final SessionFactory sessionFactory = HibarnateUtils.getSessionFactory();
        final Session session = sessionFactory.openSession();
        session.beginTransaction();

        UserDetails userDetails = new UserDetails();
        userDetails.setUsurname("blabla123");

        Address address = new Address();
        address.setCity("Lublin");
        userDetails.setAddress(address);

        Phone phone1 = new Phone();
        Phone phone2 = new Phone();
        Phone phone3 = new Phone();

        phone1.setPhoneNumber(456789321L);
        phone2.setPhoneNumber(454786548L);
        phone3.setPhoneNumber(456215842L);

        List<Phone> phones = new ArrayList<>();
        phones.add(phone1);
        phones.add(phone2);
        phones.add(phone3);

        userDetails.setPhone(phones);
        phone1.setUserDetails(userDetails);
        phone2.setUserDetails(userDetails);
        phone3.setUserDetails(userDetails);

        session.save(address);
        session.save(userDetails);
        session.save(phone1);
        session.save(phone2);
        session.save(phone3);
        session.getTransaction().commit();

        Query from_userDetails = session.createQuery("FROM UserDetails");
        List<UserDetails> resultList = from_userDetails.getResultList();

        resultList.forEach(userDetails1 -> System.out.println(userDetails.getUsurname()));


        session.close();
        HibarnateUtils.shutDownSessionFactory();
    }
}
