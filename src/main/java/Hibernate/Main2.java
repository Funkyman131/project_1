package Hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class Main2 {
    public static void main(String[] args) {

        Worker worker = new Worker();
        worker.setId(1);
        worker.setFirstName("Mirek");
        worker.setSurname("Kowalski");
        worker.setPosition("Handlarz");
        worker.setSalary(3000);
        worker.setYearsOfWork(10);


        final SessionFactory sessionFactory = HibarnateUtils.getSessionFactory();
        final Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(worker);

        Worker myWorker = session.find(Worker.class, 1L);
        System.out.println("imie" + myWorker.getFirstName());
        System.out.println("nazwisko" + myWorker.getSurname());
        session.getTransaction().commit();

        session.close();
        HibarnateUtils.shutDownSessionFactory();
    }
}
