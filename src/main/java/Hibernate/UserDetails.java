package Hibernate;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserDetails {

    @Id
    @GeneratedValue
    private int id;
    private String usurname;
    @OneToOne
    private Address address;
    @OneToMany(mappedBy = "userDetails")
    private List<Phone> phone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsurname() {
        return usurname;
    }

    public void setUsurname(String usurname) {
        this.usurname = usurname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Phone> getPhone() {
        return phone;
    }

    public void setPhone(List<Phone> phone) {
        this.phone = phone;
    }
}
