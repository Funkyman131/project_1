package Hibernate;


import javax.persistence.*;

@Entity
public class Phone {

    @Id
    @GeneratedValue
    private int id;

    private long phoneNumber;
    @ManyToOne
    private UserDetails userDetails;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
}
