package Hibernate;

import javax.persistence.*;

@Entity
@Table(name = "Dane_Pracownika")
@SecondaryTable(name = "Dane_Firmowe", pkJoinColumns = @PrimaryKeyJoinColumn(name = "worker_id"))
public class Worker {

    @Id
    @GeneratedValue
    private long id;
    @Column(name = "Imię")
    private String firstName;
    @Column(name = "Nazwisko")
    private String surname;
    @Column(name = "Stanowisko")
    private String position;
    @Column(name = "Lata_pracy", table = "Dane_Firmowe")
    private int yearsOfWork;
    @Column(name = "Pensja",table = "Dane_Firmowe")
    private int salary;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getYearsOfWork() {
        return yearsOfWork;
    }

    public void setYearsOfWork(int yearsOfWork) {
        this.yearsOfWork = yearsOfWork;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
