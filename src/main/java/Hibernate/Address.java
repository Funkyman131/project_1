package Hibernate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Address {

    @Id
    @GeneratedValue
    private int id;
    private String city;
    private String street;
    @OneToOne
    private UserDetails UserDetails;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Hibernate.UserDetails getUserDetails() {
        return UserDetails;
    }

    public void setUserDetails(Hibernate.UserDetails userDetails) {
        UserDetails = userDetails;
    }
}
