package Hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class HibarnateUtils {

    private static final SessionFactory sessionFactory = createSessionFactory();

    private static SessionFactory createSessionFactory(){
        return new Configuration().configure(new File("C:\\Users\\Adrian\\IdeaProjects\\Hibernate\\src\\main\\resources\\Hibernate.cfg.xml")).buildSessionFactory();
    }
    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public static void shutDownSessionFactory(){
        sessionFactory.close();
    }
}
